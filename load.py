import numpy as np
import scipy.io
from TOMP import TOMP
from Basis import Basis
from scipy import signal

def align_and_sub(t, x, d):
    #print("Hello from align_and_sub!")
    y = t.calc(x)
    padding = (d.shape[0]-y.shape[0]) // 2
    
    return y - d[padding:-padding]


h = np.load('save_all_h')
gamma = np.load('save_all_gamma')

path_to_data = './initial_data/'
data = scipy.io.loadmat(path_to_data+'BlackBoxData.mat')
x = data['x'][0]/2**15
y = data['y'][0]/2**15  # выходной сигнал из усилителя
d = x - y  # поскольку необходимо моделировать ошибку отн. x
data_2 = scipy.io.loadmat(path_to_data+'h_conv.mat')
h_conv = data_2['h_conv'][0]
d = np.convolve(d, h_conv, 'same')

P = 8
mode = 'ordinary'
left_bound = 0.
right_bound = max(abs(x))

R = 5
M = 2
q = 5
beta = np.ones([R, 2 * q + 1], dtype=complex)/(2*q+1)
gamma = np.ones([R, M, 2 * q + 1], dtype=complex)/(2*q+1)
#h = np.ones([R, M, P], dtype=complex)/(R*P*M)


b = Basis(p=P, mode=mode, left_bound=left_bound, right_bound=right_bound)
t = TOMP(R=R, M=M, P=P, Basis=b, h_conv = h_conv, d=d, x=x, beta=beta, gamma=gamma, h=h)

diff = align_and_sub(t, x, d)
error = 10 * np.log(np.linalg.norm(diff) / np.linalg.norm(x))

t.draw(signal=diff, label='haha')

print(error)
