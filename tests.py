from Basis import Basis
from TOMP import TOMP
import json
import scipy.io
import numpy as np


def _generate_initial_points(strategy, R, P, M, q):
    if strategy == 'ones':
        beta = np.ones([R, 2 * q + 1])
        gamma = np.ones([R, M, 2 * q + 1])
        h = np.ones([R, M, P])
        return beta, gamma, h
    else:
        print('Unknown strategy')

#test_list = [test1 = {parameter1, parameter2, ..., phases = [opt_phase1 = [method_name, method_param1, method_param2, ...], opt_phase2, ...]}, test2, ...]




#mode for option whether to print or not
def run_test(test_file, mode):
    with open(test_file, 'r') as json_file:
        test_list = json.load(json_file)
    result = []
    for test_item in test_list:
        test_result = []
        R = test_item['R']
        P = test_item['P']
        M = test_item['M']
        q = test_item['q']
        basis_type = test_item['basis_type']
        FIR_file = test_item['FIR_file']
        data_file = test_item['data_file']
        strategy = test_item['start_point_strategy']

        beta, gamma, h = _generate_initial_points(strategy, R, P, M, q)

        data = scipy.io.loadmat(data_file)
        x = data['x'][0]/2**15
        y = data['y'][0]/2**15  # выходной сигнал из усилителя
        d = x - y  # поскольку необходимо моделировать ошибку отн. x
        FIR_data = scipy.io.loadmat(FIR_file)
        h_conv = FIR_data['h_conv'][0]
        d = np.convolve(d, h_conv, 'same')

        left_bound = 0.
        right_bound = max(abs(x))


        b = Basis(P=P, mode=basis_type, left_bound=left_bound, right_bound=right_bound)
        t = TOMP(R=R, M=M, P=P, Basis=b, h_conv = h_conv, d=d, x=x, beta=beta, gamma=gamma, h=h)


        for test_phase in test_item[2]:
            res = t.opt(*test_phase)
            test_result.append(res)
        result.append(test_result)

    return result


def _make_test():
    test_list = []
    test_name = input("Enter the name of the test: ")
    num_tests = input("Enter a number of test: ")

    for i in range(int(num_tests)):
        test = {}
        test['R'] = 20
        test['P'] = 8
        test['M'] = 2
        test['q'] = 2
        test['start_point_strategy'] = 'ones'
        test['basis_type'] = 'ordinary'
        test['left_bound'] = 0.
        test['right_bound'] = 0.56
        test['FIR_file'] = './initial_data/h_conv.mat'
        test['phases'] = [[]]
        test_list.append(test)
    with open(test_name+".json", 'w') as json_file:
        json.dump(test_list, json_file, indent=4)


if __name__ == '__main__':
    _make_test()