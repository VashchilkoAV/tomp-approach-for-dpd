import numpy as np
import functools as fc
from scipy import signal
from scipy.signal import convolve
import time
import json
import copy
import matplotlib.pyplot as plt


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()       
        
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print ('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result    
    return timed

class CallBackError(Exception):
    """Base class for call_back exceptions"""
    pass

class TOMP:
    def __init__(self, R, M, P, Basis, h_conv, d, x, beta, gamma, h, norm=np.linalg.norm, is_quasi=False):
        self.h_conv = h_conv
        self.R = R
        self.M = M
        self.P = P
        self.Basis = Basis
        self.d = d
        self.x = x
        self.beta = beta
        self.gamma = gamma
        self.h = h
        self.norm = norm
        self.opt_methods = ['default' ,'momentum', 'Nesterov']
        self.error = self.calc_error(x=x, y=d)
        self.error_trace = []
        self.grad_norm = float('inf')
        self.is_quasi = is_quasi
        
    def _FIR(self, x):
        return np.convolve(x, self.h_conv, mode='valid')
    
    def calc_single(self, x, r):
        res = convolve(x, self.beta[r], mode='valid')
        for m in range(self.M):
            tmp = np.convolve(np.dot(self.F, self.h[r][m]), self.gamma[r][m], mode='valid')
            res *= tmp

        return self._FIR(res)
    
    def calc(self, x):
        self.F = self.Basis.matrix(x)
        res = np.zeros(x.shape[0]-len(self.h_conv)+2-self.beta[0].shape[0], dtype=complex)
        for r in range(self.R):
            tmp = self.calc_single(x, r)
            res += tmp
        self.last_calc = res
        return self.last_calc

    def calc_error(self, x, y):
        return 10 * np.log(self.norm(y) / self.norm(x))
    
    def _draw_signal(self, x, nperseg, label):
        f, P = signal.welch(x, nperseg=nperseg)
        f, P = zip(*sorted(zip(f, P)))
        plt.plot(1000 * np.array(f), 10 * np.log(P), linewidth = 1.0, label = label)
    
    def draw(self, signal=None, label=None):
        plt.figure(num = None, figsize = (6, 4), dpi = 250)

        self._draw_signal(self.x, 2048, 'x')
        self._draw_signal(self.d, 2048, 'd')
        if signal is not None:
            self._draw_signal(signal, 2048, label)
        
        plt.legend()
        plt.grid()
        plt.show()

    #add reduction for all np.muptiply and * and + operations in order to enable different delays
    def derivative_h(self, x, r, m, p):
        r1 = np.convolve(x, self.beta[r], mode='valid')
        r1 *= np.convolve(np.array(self.Basis.matrix(x))[:,p], self.gamma[r][m], mode='valid')
        for m_ in range(self.M):
            if m_ != m:
                tmp = np.convolve(np.dot(self.F, self.h[r][m_]), self.gamma[r][m_], mode='valid')
                r1 *= tmp
        
        return self._FIR(r1)
    
    def grad_h(self, x):
        return np.array(
        [
            [
                [
                    self.derivative_h(x=x, r=r, m=m, p=p)
                for p in range(self.P)]
            for m in range(self.M)]
        for r in range(self.R)])
    
    
    def derivative_beta(self, x, r, q):
        size_beta = self.beta[r].shape[0]
        if -(size_beta-q-1)!=0:
            r2 = copy.copy(x[q:-(size_beta-q-1)])
        else:
            r2 = copy.copy(x[q:])
        for m in range(self.M):
            tmp = np.convolve(np.dot(self.F, self.h[r][m]), self.gamma[r][m], mode='valid')
            r2 *= tmp

        return self._FIR(r2)#no minus
    
    def grad_beta(self, x):
        return np.array(
        [
            [
                self.derivative_beta(x=x, r=r, q=q) 
            for q in range(self.beta[r].shape[0])]
        for r in range(self.R)])
    

    def derivative_gamma(self, r, m, q, x):
        r1 = np.convolve(x, self.beta[r], mode='valid')
        size_gamma=self.gamma[r][m].shape[0]
        if -(size_gamma-q-1)!=0:
            r2 = x[q:-(size_gamma-q-1)]
        else:
            r2 = x[q:]
        r1 *= r2
        
        for m_ in range(self.M):
            if m_ != m:
                tmp = np.convolve(np.dot(self.F, self.h[r][m]), self.gamma[r][m], mode='valid')
                r1 *= tmp
        
        return self._FIR(r1)
    
    def grad_gamma(self, x):
        #print("Hello from grad_gamma!")
        return np.array(
        [
            [
                [
                    self.derivative_gamma(r=r, m=m, q=q, x=x)
                for q in range(self.gamma[r][m].shape[0])]
            for m in range(self.M)]
        for r in range(self.R)])
    
    def gradient(self, x, y):
        #print("Hello from gradient!")
        G = self.grad(x)
        G_conj = np.conjugate(G)
        if self.is_quasi:
            gradient = np.tensordot(np.tensordot(np.linalg.pinv(np.tensordot(G_conj, G)), G_conj), y, [[self.pos], [0]])
        else:
            gradient = np.tensordot(G_conj, y, [[self.pos], [0]])
        
        return gradient

    def align_and_sub(self, x, d):
        #print("Hello from align_and_sub!")
        y = self.calc(x)
        if d.shape[0] != y.shape[0]:
            padding = (d.shape[0]-y.shape[0]) // 2
            return y - d[padding:-padding]
        else:
            return y - d
    
    def update_parameters(self, x):
        #print("Hello from update_parameters!")
        if self.gd_mode == 'h':
            self.h = x
        elif self.gd_mode == 'beta':
            self.beta = x
        elif self.gd_mode == 'gamma':
            self.gamma = x
    
    def _grad_step(self, x, d):
        #print("Hello from _grad_step!")
        diff = self.align_and_sub(x, d)
        self.error = self.calc_error(x=x, y=diff)
        self.error_trace.append(self.error)
        grad = self.gradient(x=x, y=diff)
        #print(x[0], d[0])
        self.grad_norm = self.norm(grad)
        print(f'Error on batch: {self.error} Grad norm: {self.grad_norm}')

        if self.callback():
            raise CallBackError("Callback in _grad_step!")
        learning_rate = self.select_learning_rate()
        
        if self.modification_mode == 'momentum':
            self.x_next = self.x_act - learning_rate * grad + self.momentum * (self.x_act - self.x_prev)
            self.x_prev = self.x_act.copy()
            self.x_act = self.x_next.copy()
        elif self.modification_mode == 'Nesterov':
            self.y_next = self.x_act - learning_rate * grad
            self.x_next = self.y_next + self.iteration / (self.iteration + 3) * (self.y_next - self.y_act)
            self.y_act = self.y_next.copy()
            self.x_act = self.x_next.copy()
        else:
            self.x_next = self.x_act - learning_rate * grad
            self.x_act = self.x_next.copy()
        
        self.update_parameters(x=self.x_act)       
          
    def grad_step_batched(self, pos):
        #print("Hello from grad_step_batched!")
        x_batched = self.x[pos * self.batch_size: min((pos+1) * self.batch_size, len(self.x))]
        d_batched = self.d[pos * self.batch_size: min((pos+1) * self.batch_size, len(self.x))]
        self.F = self.Basis.matrix(x_batched)
        try:
            for i in range(self.steps_per_batch):
                self._grad_step(x=x_batched, d=d_batched)
        except CallBackError as e:
            print(e.args[0])
                
    def save_after_descent(method):
        def magic(self, *args, **kwargs):
            result = method(self, *args, **kwargs)
            with open("save_"+self.gd_mode, 'wb') as file:
                np.save(file, self.x_best)
            return result    
        return magic

    @save_after_descent
    def gradient_descent(self, epsilon, num_iter, gd_mode, modification_mode, step_mode, callback_mode, batch_size, steps_per_batch, is_rand, disp, **kwargs):
        #print("Hello from gradient_descent!")
        self.args = kwargs
        self.epsilon = epsilon
        self.num_iter = num_iter
        
        self.gd_mode = gd_mode
        self.modification_mode = modification_mode
        self.step_mode = step_mode
        self.callback_mode = callback_mode
        
        self.batch_size = batch_size 
        self.steps_per_batch = steps_per_batch
        self.is_rand = is_rand
        
        self.init_for_gd()
        
        self.iteration = 0
        while True:
            # Randomly chosen batch
            if self.is_rand:
                pos = np.random.randint(0, len(self.x) // self.batch_size)
                self.grad_step_batched(pos)
            else:
                for i in range(len(self.x) // batch_size):
                    self.grad_step_batched(i)
            self.iteration += 1
            if disp:
                print(f"Iterations made: {self.iteration}")
                print(f"Current gradient norm {self.grad_norm}")
            if self.iteration >= num_iter:
                break
            
        res = {self.gd_mode: self.x_best, "num_iter": self.iteration}
        return res
    
    def init_for_gd(self):
        #print("Hello from init_for_gd!")
        parameters = {
            'gd_mode': {
                'h': {
                    'grad': self.grad_h,
                    'x_act': self.h.copy(),
                    'x_best': self.h.copy(),
                    'pos': 3,
                },
                'beta': {
                    'grad': self.grad_beta,
                    'x_act': self.beta.copy(),
                    'x_best': self.beta.copy(),
                    'pos': 2,
                },
                'gamma': {
                    'grad': self.grad_gamma,
                    'x_act': self.gamma.copy(),
                    'x_best': self.gamma.copy(),
                    'pos': 3,
                },
            },
            'step_mode': {
                'const',
                'sequence',
            },
            'callback_mode': {
                'default': self.default_callback,
                'n_step': self.n_step_callback,
                'n_decrease_step_callback': self.n_decrease_step_callback,
            },
        }
        
        # GD parameter verification
        if not isinstance(self.epsilon, float) or self.epsilon <= 0:
            raise ValueError("'epsilon' parameter must be float greater than 0!")
        if not isinstance(self.num_iter, int) or self.num_iter < 1:
            raise ValueError("'num_iter' parameter must be integer greater than 0!")
        if not isinstance(self.batch_size, int) or self.batch_size < 1 or self.batch_size > len(self.x):
            raise ValueError("'batch_size' parameter must be integer greater than 0 and less than 'x' size!")
        if not isinstance(self.steps_per_batch, int) or self.steps_per_batch < 1:
            raise ValueError("'steps_per_batch' parameter must be integer greater than 0!")
        if not isinstance(self.is_rand, bool):
            raise ValueError("'is_rand' parameter must be bool!")
        
        if self.gd_mode in parameters['gd_mode']:
            parameters_ = parameters['gd_mode'][self.gd_mode]
            self.grad = parameters_['grad']
            self.x_act = parameters_['x_act']
            self.x_best = parameters_['x_best']
            self.pos = parameters_['pos']
        else:
            raise ValueError(f"There is no such parameter for GD: {self.gd_mode}!\n"
                             f"Available parameters for GD: {parameters['gd_mode'].keys()}")

        parameters['modification_mode'] = {
            'default': {},
            'momentum': {
                'x_prev': self.x_act.copy(),
            },
            'Nesterov': {
                'y_act': self.x_act.copy(),
            },
        }
            
            
        # Modification mode verification
        if self.modification_mode in parameters['modification_mode']:
            parameters_ = parameters['modification_mode']
            if self.modification_mode == 'momentum':
                if not 'momentum' in self.args:
                    raise SyntaxError("You must specify 'momentum' argument for GD momentum!")
                self.momentum = self.args['momentum']
                self.x_prev = parameters_['momentum']['x_prev']
            elif self.modification_mode == 'Nesterov':
                self.y_act = parameters_['Nesterov']['y_act']
        else:
            raise ValueError(f"There is no such modification for GD: {self.modification_mode}!\n"
                             f"Available parameters for GD modification: {parameters['modification_mode'].keys()}")
            
        # Step mode verification
        if self.step_mode in parameters['step_mode']:
            if self.step_mode == 'const':
                if not 'learning_rate' in self.args:
                    raise SyntaxError("You must specify 'learning_rate' argument for 'const' step mode!")
            elif self.step_mode == 'sequence':
                if not 'sequence' in self.args:
                    raise SyntaxError("You must specify 'sequence' argument (function of learning_rate and k) for 'sequence' step mode!")
                if not 'learning_rate' in self.args:
                    raise SyntaxError("You must specify 'learning_rate' for 'sequence' step mode!")
        else:
            raise ValueError(f"There is no specified step mode: {self.step_mode}!\n"
                             f"Available parameters for step modification: {parameters['step_mode'].keys()}")
        
        # Callback verification
        if self.callback_mode in parameters['callback_mode']:
            parameters_ = parameters['callback_mode']
            if self.callback_mode == 'default':
                self.callback = parameters_['default']
            elif self.callback_mode == 'n_step':
                if not 'n' in self.args:
                    raise SyntaxError("You must specify 'n' for 'n_step' callback!")
                self.n = self.args['n']
                if not isinstance(self.n, int) or self.n < 2:
                    raise ValueError("'n' parameter must be integer greater than 1!")
                self.callback = parameters_['n_step']
            elif self.callback_mode == 'n_decrease_step_callback':
                if not 'n' in self.args:
                    raise SyntaxError("You must specify 'n' for 'n_decrease_step' callback!")
                self.n = self.args['n']
                if not isinstance(self.n, int) or self.n < 2:
                    raise ValueError("'n' parameter must be integer greater than 1!")
                if not 'fact' in self.args:
                    raise SyntaxError("You must specify 'fact' for 'n_decrease_step' callback!")
                self.fact = self.args['fact']
                if not isinstance(self.fact, float) or self.fact <= 0:
                    raise ValueError("'fact' parameter must be float greater than 0!")
                
                self.callback = parameters_['n_decrease_step_callback']
        else:
            raise ValueError(f"There is no specified callback mode: {self.callback_mode}!\n"
                             f"Available parameters for callback modification: {parameters['callback_mode'].keys()}")
    
    def select_learning_rate(self):
        #print("Hello from select_learning_rate!")
        if self.step_mode == 'const':
            return self.args['learning_rate']
        elif self.step_mode == 'sequence':
            return self.args['sequence'](self.args['learning_rate'], self.iteration)

    def default_callback(self):
        if self.error == float('inf'):
            self.error_trace = []
            return True
        return False
        
    def n_step_callback(self):
        if self.error == float('inf'):
            self.error_trace = []
            return True
        if len(self.error_trace) == self.n:
            i = 0
            j = 1
            while j < self.n:
                if self.error_trace[j] < self.error_trace[i]:
                    self.error_trace = []
                    return False
                i += 1
                j += 1
            self.error_trace = []
            return True
        return False

    def n_decrease_step_callback(self):
        if self.error == float('inf'):
            self.error_trace = []
            return True
        if len(self.error_trace) == self.n:
            i = 0
            j = 1
            while j < self.n:
                if self.error_trace[j] < self.error_trace[i]:
                    self.error_trace = []
                    return False
                i += 1
                j += 1
            self.error_trace = []
            self.args['learning_rate'] /= self.fact
            return True
        return False

    def get_params(self):
        return self.h, self.beta, self.gamma