import matplotlib.pyplot as plt
import numpy as np


class Basis():
    """Making coefficient array of given polynomial"""
    
    def __init__(self, p=3, mode='chebyshev', left_bound=0.0, right_bound=1.0):
        self.mode = mode
        self.P = p
        self.coef = []
        self.left_bound = left_bound
        self.right_bound = right_bound
        if (mode == 'chebyshev'):
            for i in range(self.P):
                coef = [0.0] * i + [1.0]
                cheb = np.polynomial.chebyshev.Chebyshev(coef=coef)
                self.coef.append(list(np.polynomial.chebyshev.cheb2poly(cheb.coef))[::-1])
                
        elif (mode == 'ordinary'):
            for i in range(self.P):
                self.coef.append([1.0]+[0.0] * i)
        
        else:
            raise ValueError(f"There is no mode: {mode}")
            
    def calc(self, p, x):
        if ((p >= self.P) or not isinstance(p, int) or p < 0):
            raise ValueError(f"Degree must be non-negative integer less than {self.P}!")
        
        if (self.mode == 'chebyshev'):
            x = 2 * (x - self.left_bound) / (self.right_bound - self.left_bound) - 1
        elif (self.mode == 'ordinary'):
            x = (x - self.left_bound) / (self.right_bound - self.left_bound)
            
        value = 0
        for i in self.coef[p]:
            value *= x
            value += i
        return value
    
    def matrix(self, x):
        F = []
        for i in range(len(x)):
            line = [0]*self.P
            for j in range(self.P):
                line[j] = self.calc(j, abs(x[i]))
            F.append(line)
            
        return F
    
    def draw(self):
        plt.figure(num = None, figsize = (6, 4), dpi = 250)
        plt.title(self.mode)
        x = np.linspace(self.left_bound, self.right_bound, 100)
        for degree in range(self.P):
            plt.plot(x, self.calc(degree, x), linewidth = 1.0, label = f'{degree}')
