import numpy as np
import scipy.io
from TOMP import TOMP
from Basis import Basis
from tests import run_test

if __name__=='__main__':
    path_to_data = './initial_data/'
    data = scipy.io.loadmat(path_to_data+'BlackBoxData.mat')
    x = data['x'][0]/2**15
    y = data['y'][0]/2**15  # выходной сигнал из усилителя
    d = x - y  # поскольку необходимо моделировать ошибку отн. x
    data_2 = scipy.io.loadmat(path_to_data+'h_conv.mat')
    h_conv = data_2['h_conv'][0]
    d = np.convolve(d, h_conv, 'same')

    P = 8
    mode = 'ordinary'
    left_bound = 0.
    right_bound = max(abs(x))

    R = 10
    M = 2
    q = 5

    beta = np.ones([R, 2 * q + 1], dtype='complex_') / (2 * q + 1)
    gamma = np.ones([R, M, 2 * q + 1], dtype='complex_') / (2 * q + 1)
    h = np.ones([R, M, P], dtype='complex_') / (R * M * P)
    np.random.seed(30)
    #beta = np.random.rand(R, 2 * q + 1)
    #gamma = np.random.rand(R, M, 2 * q + 1)
    #h = np.random.rand(R, M, P)
    h_best = h.copy()
    beta_best = beta.copy()
    gamma_best = gamma.copy()

    b = Basis(p=P, mode=mode, left_bound=left_bound, right_bound=right_bound)
    for epoch in range(R):
        epoch += 1
        print(f'Epoch: {epoch}')
        t = TOMP(R=epoch, M=M, P=P, Basis=b, h_conv = h_conv, d=d, x=x, beta=beta[:epoch], gamma=gamma[:epoch], h=h[:epoch])
        print('h optimization:')
        gd = t.gradient_descent(epsilon=0.1, num_iter=2, gd_mode='h', modification_mode='momentum', momentum=0.9, step_mode='const', learning_rate=0.0001, callback_mode='n_step', n=2, batch_size=500, steps_per_batch=5,  is_rand=False, disp=True)
        h_best = gd['h'].copy()
        print(gd)
        '''print('Beta optimization:')
        gd = t.gradient_descent(gd_mode='beta', learning_rate=0.0001, callback_mode='n_step', n=2, epsilon=1, num_iter=2, batch_size=5000, steps_per_batch=20, disp=True, step_mode='const')
        beta_best = gd['beta'].copy()
        print(gd)
        print('Gamma optimization:')
        gd = t.gradient_descent(gd_mode='gamma', learning_rate=0.0001, callback_mode='n_step', n=2, epsilon=1, num_iter=2, batch_size=5000, steps_per_batch=20, disp=True, step_mode='const')
        gamma_best = gd['gamma'].copy()
        h[:epoch] = h_best.copy()
        beta[:epoch] = beta_best.copy()
        gamma[:epoch] = gamma_best.copy()'''
        d = t.align_and_sub(x=x, d=d)
        print(f'Error: {10 * np.log(t.norm(d)/t.norm(x))}')
        print(f'd.shape: {d.shape}')
        print(f'y.shape: {y.shape}')
    print(h, beta, gamma)
