from os import error
import numpy as np
import functools as fc
from scipy import signal
from scipy.signal import convolve
import time
import json
import copy
import matplotlib.pyplot as plt
import scipy.io

import matplotlib.pyplot as plt
import numpy as np



def align_and_sub(t, x, d):
    #print("Hello from align_and_sub!")
    y = t.calc(x)
    padding = (d.shape[0]-y.shape[0]) // 2
    
    return y - d[padding:-padding]


class Basis():
    """Making coefficient array of given polynomial"""
    
    def __init__(self, p=3, mode='chebyshev', left_bound=0.0, right_bound=1.0):
        self.mode = mode
        self.P = p
        self.coef = []
        self.left_bound = left_bound
        self.right_bound = right_bound
        if (mode == 'chebyshev'):
            for i in range(self.P):
                coef = [0.0] * i + [1.0]
                cheb = np.polynomial.chebyshev.Chebyshev(coef=coef)
                self.coef.append(list(np.polynomial.chebyshev.cheb2poly(cheb.coef))[::-1])
                
        elif (mode == 'ordinary'):
            for i in range(self.P):
                self.coef.append([1.0]+[0.0] * i)
        
        else:
            raise ValueError(f"There is no mode: {mode}")
            
    def calc(self, p, x):
        if ((p >= self.P) or not isinstance(p, int) or p < 0):
            raise ValueError(f"Degree must be non-negative integer less than {self.P}!")
        
        if (self.mode == 'chebyshev'):
            x = 2 * (x - self.left_bound) / (self.right_bound - self.left_bound) - 1
        elif (self.mode == 'ordinary'):
            x = (x - self.left_bound) / (self.right_bound - self.left_bound)
            
        value = 0
        for i in self.coef[p]:
            value *= x
            value += i
        return value
    
    def matrix(self, x):
        F = []
        for i in range(len(x)):
            line = [0]*self.P
            for j in range(self.P):
                line[j] = self.calc(j, abs(x[i]))
            F.append(line)
            
        return np.array(F)
    
    def draw(self):
        plt.figure(num = None, figsize = (6, 4), dpi = 250)
        plt.title(self.mode)
        x = np.linspace(self.left_bound, self.right_bound, 100)
        for degree in range(self.P):
            plt.plot(x, self.calc(degree, x), linewidth = 1.0, label = f'{degree}')





class CallBackError(Exception):
    """Base class for call_back exceptions"""
    pass

class TOMP:
    def __init__(self, R, M, P, Basis, h_conv, d, x, beta, gamma, h, norm=np.linalg.norm, is_quasi=False):
        self.h_conv = h_conv
        self.R = R
        self.M = M
        self.P = P
        self.Basis = Basis
        self.d = d
        self.x = x
        self.beta = beta
        self.gamma = gamma
        self.h = h
        self.norm = norm
        self.error = self.calc_error(x=x, y=d)
        self.grad_norm = float('inf')
        
    def _FIR(self, x):
        return np.convolve(x, self.h_conv[::-1], mode='valid')
    
    def calc_single(self, x, r):
        res = convolve(x, self.beta[r][::-1], mode='valid')
        for m in range(self.M):
            tmp = np.convolve(np.dot(self.F, self.h[r][m]), self.gamma[r][m][::-1], mode='valid')
            res *= tmp

        return self._FIR(res)
    
    def calc(self, x):
        self.F = self.Basis.matrix(x)
        res = np.zeros(x.shape[0]-len(self.h_conv)+2-self.beta[0].shape[0], dtype=complex)
        for r in range(self.R):
            tmp = self.calc_single(x, r)
            res += tmp
        self.last_calc = res
        return self.last_calc

    def calc_error(self, x, y):
        return 10 * np.log(self.norm(y) / self.norm(x))

    #add reduction for all np.muptiply and * and + operations in order to enable different delays
    def derivative_h(self, x, r, m, p):
        r1 = np.convolve(x, self.beta[r][::-1], mode='valid')
        r1 *= np.convolve(self.F[:,p], self.gamma[r][m][::-1], mode='valid')
        for m_ in range(self.M):
            if m_ != m:
                tmp = np.convolve(np.dot(self.F, self.h[r][m_]), self.gamma[r][m_][::-1], mode='valid')
                r1 *= tmp
        
        return self._FIR(r1)
    
    def grad_h(self, x):
        #print(x[0])
        return np.array(
        [
            [
                [
                    self.derivative_h(x=x, r=r, m=m, p=p)
                for p in range(self.P)]
            for m in range(self.M)]
        for r in range(self.R)])

    def gradient_h(self, x, y):
        G = self.grad_h(x)
        G_conj = np.conjugate(G)
        gradient = np.tensordot(G_conj, y, [[3], [0]])

        return gradient

    def align_and_sub(self, x, d):
        #print("Hello from align_and_sub!")
        y = self.calc(x)
        if d.shape[0] != y.shape[0]:
            padding = (d.shape[0]-y.shape[0]) // 2
            return y - d[padding:-padding]
        else:
            return y - d
    
    
    def _grad_step(self, x, d, n_iter):
        #print("Hello from _grad_step!")
        print(n_iter)
        for i in range(self.steps_per_batch):
            diff = self.align_and_sub(x, d)
            self.error = self.calc_error(x=x, y=diff)
            if self.gd_mode == 'h':
                grad = self.gradient_h(x=x, y=diff)
                self.grad_norm = self.norm(grad)
                #print(f'Global iteration: {n_iter}. N_iter_in_batch: {i}. Error on batch: {self.error} Grad norm: {self.grad_norm}')
                self.h = self.h - self.alpha*grad

         
                
    def gradient_descent(self, lr, num_iter, gd_mode, batch_size, steps_per_batch):
        print('hey')
        self.num_iter = num_iter
        self.alpha = lr

        self.gd_mode = gd_mode
        
        self.batch_size = batch_size 
        self.steps_per_batch = steps_per_batch
        
        pos = np.random.randint(0, len(self.x) // self.batch_size)
        x_batched = copy.copy(self.x[pos * self.batch_size: min((pos+1) * self.batch_size, len(self.x))])
        d_batched = copy.copy(self.d[pos * self.batch_size: min((pos+1) * self.batch_size, len(self.x))])
        self.F = self.Basis.matrix(x_batched)


        for i in range(num_iter):
            self._grad_step(x_batched, d_batched, i)
       
    
    
path_to_data = './initial_data/'
data = scipy.io.loadmat(path_to_data+'BlackBoxData.mat')
x = data['x'][0]/2**15
y = data['y'][0]/2**15  # выходной сигнал из усилителя
d = x - y  # поскольку необходимо моделировать ошибку отн. x
data_2 = scipy.io.loadmat(path_to_data+'h_conv.mat')
h_conv = data_2['h_conv'][0]
d = np.convolve(d, h_conv, 'same')

P = 8
mode = 'ordinary'
left_bound = 0.
right_bound = max(abs(x))

R = 10
M = 2
q = 5

beta = np.empty([R, 2*q+1])
for i in range(R):
    a = np.random.random(2*q+1)
    a /= a.sum()
    beta[i] = a


gamma = np.empty([R, M, 2*q+1])
for i in range(R):
    for j in range(M):
        a = np.random.random(2*q+1)
        a /= a.sum()
        gamma[i][j] = a

h = np.empty([R, M, P])
for i in range(R):
    for j in range(M):
        a = np.random.random(P)
        a /= a.sum()
        h[i][j] = a

#beta = np.ones([R, 2 * q + 1], dtype=complex)/(2*q+1)
#gamma = np.ones([R, M, 2 * q + 1], dtype=complex)/(2*q+1)
#h = np.ones([R, M, P], dtype=complex)/(R*P*M)
#print(h)


b = Basis(p=P, mode=mode, left_bound=left_bound, right_bound=right_bound)
t = TOMP(R=R, M=M, P=P, Basis=b, h_conv = h_conv, d=d, x=x, beta=beta, gamma=gamma, h=h)

diff_init = align_and_sub(t, x, d)
error_init = 10 * np.log(np.linalg.norm(diff_init) / np.linalg.norm(x))


#0.05, 2000
gd = t.gradient_descent(gd_mode='h', lr=0.005, num_iter=200, batch_size=2000, steps_per_batch=100)

diff = align_and_sub(t, x, d)
error = 10 * np.log(np.linalg.norm(diff) / np.linalg.norm(x))

y = t.calc(x)


with open('save10', 'wb') as f:
    np.save(f, y)

f_x, P_x = signal.welch(x, nperseg = 2048)
f_x, P_x = zip(*sorted(zip(f_x, P_x)))

f_d, P_d = signal.welch(d, nperseg = 2048)
f_d, P_d = zip(*sorted(zip(f_d, P_d)))

f_y, P_y = signal.welch(diff, nperseg = 2048)
f_y, P_y = zip(*sorted(zip(f_y, P_y)))

#f_yi, P_yi = signal.welch(diff_init, nperseg = 2048)
#f_yi, P_yi = zip(*sorted(zip(f_yi, P_yi)))

plt.figure(num = None, figsize = (6, 4), dpi = 250)

plt.plot(1000 * np.array(f_x), 10 * np.log(P_x), linewidth = 1.0, label = 'x')
plt.plot(1000 * np.array(f_d), 10 * np.log(P_d), linewidth = 1.0, label = 'd')
plt.plot(1000 * np.array(f_y), 10 * np.log(P_y), linewidth = 1.0, label = 'y')
#plt.plot(1000 * np.array(f_yi), 10 * np.log(P_yi), linewidth = 1.0, label = 'y_init')
plt.grid()
plt.show()
plt.savefig('r10')

print(error)