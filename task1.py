import numpy as np
import scipy.io
from TOMP import TOMP
from Basis import Basis

def align_and_sub(t, x, d):
    #print("Hello from align_and_sub!")
    y = t.calc(x)
    padding = (d.shape[0]-y.shape[0]) // 2
    
    return y - d[padding:-padding]


if __name__=='__main__':
    path_to_data = './initial_data/'
    data = scipy.io.loadmat(path_to_data+'BlackBoxData.mat')
    x = data['x'][0]/2**15
    y = data['y'][0]/2**15  # выходной сигнал из усилителя
    d = x - y  # поскольку необходимо моделировать ошибку отн. x
    data_2 = scipy.io.loadmat(path_to_data+'h_conv.mat')
    h_conv = data_2['h_conv'][0]
    d = np.convolve(d, h_conv, 'same')

    P = 8
    mode = 'ordinary'
    left_bound = 0.
    right_bound = max(abs(x))

    R = 5
    M = 2
    q = 5
    beta = np.ones([R, 2 * q + 1], dtype=complex)/(2*q+1)
    gamma = np.ones([R, M, 2 * q + 1], dtype=complex)/(2*q+1)
    h = np.ones([R, M, P], dtype=complex)/(R*P*M)
    #print(h)


    b = Basis(p=P, mode=mode, left_bound=left_bound, right_bound=right_bound)
    t = TOMP(R=R, M=M, P=P, Basis=b, h_conv = h_conv, d=d, x=x, beta=beta, gamma=gamma, h=h)

    #diff = align_and_sub(t, x, d)
    #error1 = 10 * np.log(np.linalg.norm(diff) / np.linalg.norm(x))

    #gd = t.gradient_descent(gd_mode='h', learning_rate=0, epsilon=10e-4, num_iter=500, batch_size=4000, step_mode='const', is_rand=True, modification_mode='momentum', momentum=0.9)


#5*10e-3
    gd = t.gradient_descent(gd_mode='h', learning_rate=1*10e-1, epsilon=10e-4, num_iter=100, batch_size=800, step_mode='const', is_rand=True, modification_mode='momentum', momentum=0.9, disp=True, steps_per_batch=200, callback_mode='n_decrease_step_callback', n=5, fact=2.)

    with open('save_all_h', 'wb') as f:
        a = t.h
        np.save(f, a)
    with open('save_all_beta', 'wb') as f:
        a = t.beta
        np.save(f, a)
    with open('save_all_gamma', 'wb') as f:
        a = t.gamma
        np.save(f, a)



    gd = t.gradient_descent(gd_mode='gamma', learning_rate=7*10e-2, epsilon=10e-4, num_iter=100, batch_size=800, step_mode='const', is_rand=True, modification_mode='momentum', momentum=0.9, disp=True, steps_per_batch=200, callback_mode='n_decrease_step_callback', n=5, fact=2.)
    
    with open('save_all_h', 'wb') as f:
        a = t.h
        np.save(f, a)
    with open('save_all_beta', 'wb') as f:
        a = t.beta
        np.save(f, a)
    with open('save_all_gamma', 'wb') as f:
        a = t.gamma
        np.save(f, a)

    gd = t.gradient_descent(gd_mode='beta', learning_rate=10, epsilon=10e-4, num_iter=100, batch_size=800, step_mode='const', is_rand=True, modification_mode='momentum', momentum=0.9, disp=True, steps_per_batch=200, callback_mode='n_decrease_step_callback', n=5, fact=2.)
    print(t.get_params())
    
    #beta_new = np.concatenate((t.beta, beta))
    #gamma_new = np.concatenate((t.gamma, gamma))
    #h_new = np.concatenate((t.h, h))
    #t.beta=beta_new
    #t.gamma=gamma_new
    #t.h=h_new
    #t.R = t.R*2
    #print(t.h)


    #diff = align_and_sub(t, x, d)
    #error3 = 10 * np.log(np.linalg.norm(diff) / np.linalg.norm(x))


    #print(error3)


